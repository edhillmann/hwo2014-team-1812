package hwo2014

import akka.actor.ActorRef

/**
 * Created by ed on 23/04/14.
 */
class InsideLanePathControl(car: ActorRef) extends PathControl(car) {

  override def defineOptimalLane(myPosition: CarPosition, otherCarPositions: List[CarPosition], switchPiece: Piece): Option[String] = {
    val nextCurve = nextCurvedPiece(switchPiece)
    var defaultDirection: String = "None"
    nextCurve.angle match {
      case x if x > 0 => defaultDirection = "Right"
      case _ => defaultDirection = "Left"
    }
    Option.apply(defaultDirection)
  }
}
