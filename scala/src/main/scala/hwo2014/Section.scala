package hwo2014

import sun.awt.geom.Curve

/**
 * Created by beisenmenger on 20/04/2014.
 */

object Section {

}

case class ShortEasy()
case class LongEasy()
case class ShortMedium()
case class LongMedium()
case class ShortHard()
case class LongHard()
case class Hairpin()

trait Section {
  type T <: Piece

  def pieces:List[T]
  var target = 0.0

  def canMergeWith(other:Section):Boolean

  def merge(other:Section):Section

  def initialTarget:Double

  def initializeTargets {
    setTarget(initialTarget)
  }

  def setTarget(speed:Double) {
    target = speed
    for {p <- pieces
         l <- p.lanes}
    {
      l.target.speed = speed
    }
  }



  def describe:String

}


object Turn {
  val SHORT_RADIUS = 100
  val EASY_ANGLE = 22.5
  val MEDIUM_ANGLE = 45
  val HARD_ANGLE = 90

  val MAX_ANGLE_VELOCITY = 3.5
  val MAX_ANGLE_VELOCITY_RADIANS = Math.toRadians(MAX_ANGLE_VELOCITY)

  val NOMINAL_FORCE = 0.36
}

class Turn(_pieces:List[CurvedPiece]) extends Section {
  type T = CurvedPiece
  def pieces = _pieces

  val angle = pieces.foldLeft(0.0)((s,p) => s + p.angle)
  val radius = pieces(0).radius

  def this(p:CurvedPiece) = this (List(p))

  override def merge(other: Section) = {
      other match {
        case s:Turn => new Turn(pieces ::: s.pieces)
      }
    }

    override def canMergeWith(other: Section) = {
      other match {
        case s:Turn  => s.angle.signum == angle.signum && s.radius == radius
        case _ => false
      }
    }

  def identifyType() = {
      if (angle.abs <= Turn.EASY_ANGLE) {
        if (radius <= Turn.SHORT_RADIUS) {
          ShortEasy()
        } else {
          LongEasy()
        }
      } else if (angle.abs <= Turn.MEDIUM_ANGLE) {
        if (radius <= Turn.SHORT_RADIUS) {
          ShortMedium()
        } else {
          LongMedium()
        }
      } else if (angle.abs <= Turn.HARD_ANGLE) {
            if (radius <= Turn.SHORT_RADIUS) {
              ShortHard()
            } else {
              LongHard()
            }
      } else {
        Hairpin
      }
    }


  override def initializeTargets = {
    //target = Turn.MAX_ANGLE_VELOCITY_RADIANS * radius
    for {p <- pieces
             l <- p.lanes}
    {
      val laneRadius = p.radiusOfLane(l)
      val maxLinearSpeed = Math.sqrt(Turn.NOMINAL_FORCE * laneRadius)
      val angle = p.angle

//      println(s"Angle $angle, Radius $laneRadius => $maxLinearSpeed")
      l.target.speed = maxLinearSpeed
    }
  }

  override def initialTarget = {
    identifyType() match {
                case LongEasy() =>  6.9
                case ShortEasy() => 6.7
                case LongMedium() => 6.2
                case ShortMedium() => 6.0
                case LongHard() => 5.8
                case ShortHard() => 5.7
                case Hairpin() => 5.7
                case _ => 5.5
              }
  }


  override def setTarget(speed: Double) = {
    if (target == 0.0) {
      super.setTarget(speed)
    }

    // for turns, only apply changes to the end of the turn
    val secondHalf = pieces.takeRight(pieces.length / 2)
    val firstHalf = pieces.take(pieces.length / 2)

    val subset = if (speed > target) {

    } else {

    }
    val diff = speed - target
    val altTarget = target + (diff / 3.0)

    target = speed
    for {p <- firstHalf
         l <- p.lanes
    } {
      l.target.speed = if (diff > 0) altTarget else target
    }

    for {p <- secondHalf
             l <- p.lanes
        } {
          l.target.speed = if (diff > 0) target else altTarget
        }

  }

  def describe = identifyType() + " " + angle + " degrees x " + radius
}

class Straight(_pieces:List[StraightPiece]) extends Section {
  type T = StraightPiece
  def pieces = _pieces

  val length = pieces.foldLeft(0.0)((s,p) => s + p.length)

  def this(p:StraightPiece) = this (List(p))


  override def merge(other: Section) = {
    other match {
      case s:Straight => new Straight(pieces ::: s.pieces)
    }
  }

  override def canMergeWith(other: Section) = {
    other match {
      case s:Straight => true
      case _ => false
    }
  }


  override def initialTarget = {
    30.0
  }

  override def describe = length + " straight"
}
