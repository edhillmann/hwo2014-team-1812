package hwo2014

import akka.actor.{Props, ActorLogging, Actor, ActorRef}

/**
 * Created by beisenmenger on 23/04/2014.
 */
class ThrottleControl(car:ActorRef) extends Actor with ActorLogging {
  var lastPosition:CarPosition = null
    var speed = 0.0   // distance units per tick
    var acceleration = 10.0
    var maxSpeed = 0.0
    var maxAcceleration = 0.0
    var maxDeceleration = 1.0

  var slipVelocity = 0.0
  var slipAcceleration = 0.0

  var throttle = 0.5

  var targetMonitor = context.actorOf(Props(new NoOpTargetMonitor()), "targetMonitor")
  var turboAvailable = false
  var turboOn = false
  var turboInfo:TurboAvailable = null
  var crashed = false

  var dimensions:CarDimensionsMsg = null


  override def receive = {
    case cd:CarDimensionsMsg => dimensions = cd
    case c:Crash =>
      targetMonitor ! c
      crashed = true
    case s:Spawn => crashed = false
    case gi:GameInit => targetMonitor ! gi
    case ta:TurboAvailable =>
      turboAvailable = true
      turboInfo = ta
    case t:TurboStart => turboOn = true
    case t:TurboEnd =>
      turboOn = false
      throttle = 0.1
    case position:CarPosition =>

      if (lastPosition != null) {
        val currentSpeed = Track.distanceBetween(lastPosition.piecePosition, position.piecePosition)

        if (!turboOn) {
          acceleration = currentSpeed - speed
          speed = currentSpeed
          maxSpeed = maxSpeed.max(speed)
            maxAcceleration = maxAcceleration.max(acceleration)

            if (acceleration < 0) {
              maxDeceleration = maxDeceleration.max(acceleration.abs)
            }
          }


        val av = position.angle - lastPosition.angle
        slipAcceleration = av - slipVelocity
        slipVelocity = av


      }
      lastPosition = position



      val currentPiece = Track.pieces(position.piecePosition.pieceIndex)
      val nextPiece = currentPiece.next

      var targetSpeed = nextPiece.next.lanes(position.piecePosition.lane.endLaneIndex).target.speed

      var speedDiff = targetSpeed - speed

      /*
      if (speedDiff < 0) {

        val breakingDistance = speedDiff / maxDeceleration
        val distanceRemaining = currentPiece.lengthOfLane(position.piecePosition.lane.endLaneIndex) - position.piecePosition.inPieceDistance

        if (distanceRemaining < breakingDistance) {
          targetSpeed = currentPiece.lanes(position.piecePosition.lane.endLaneIndex).target.speed
          speedDiff = targetSpeed - speed
        }
      }
      */

      val speedDiffPercent = (speedDiff / targetSpeed).abs


      if (speedDiff > 0) { // below speed
        val max = currentPiece match {
          case s:StraightPiece => 1.0
          case _ => 1.0
        }
        throttle = (throttle * (1.0 + speedDiffPercent)).min(max)
      } else if (speedDiff < 0) { // above speed

        throttle = (throttle * (1.0 - speedDiffPercent)).max(0.01)
      }


      if (!crashed && turboAvailable && isSuitableForTurbo(position)) {
        log.info("TURBO is go")
        car ! Turbo("turbo", "Yeah baby!")
        turboAvailable = false
      }

        car ! SetThrottle(throttle)
    targetMonitor ! (position -> speed)
  }

  def isCorner(piece:Piece) = {
    piece match {
      case c:CurvedPiece => true
      case _ => false
    }
  }

  def isSuitableForTurbo(position:CarPosition) = {
    if (position.angle.abs > 20) false
    else {
      val turboTicks = turboInfo.data.turboDurationTicks.get
      val distanceRequired = turboTicks * maxSpeed * (1.0 + turboInfo.data.turboFactor / 10.0)

      var piece = Track.pieces(position.piecePosition.pieceIndex)

      var distanceAvailable = piece.lengthOfLane(position.piecePosition.lane.endLaneIndex) - position.piecePosition.inPieceDistance
      piece = piece.next
      while (!isCorner(piece)) {
        distanceAvailable = distanceAvailable + piece.lengthOfLane(position.piecePosition.lane.endLaneIndex)
        piece = piece.next
      }

      // todo Should always do it on the final straight of the last lap if possible
      distanceAvailable > distanceRequired
    }
  }

}
