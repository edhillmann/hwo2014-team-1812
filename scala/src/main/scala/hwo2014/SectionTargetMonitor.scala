package hwo2014

import akka.actor.{ActorLogging, Actor}

/**
 * Created by beisenmenger on 20/04/2014.
 */
class SectionTargetMonitor extends Actor with ActorLogging {
  var currentSection:Section = null
  var currentLane = -1
  var maxSpeed = 0.0
  var maxSlippage = 0.0
  var crash = false

  var sections = List[Section]()
  var pieceMap = Map[Piece, Section]()

  override def receive = {
    case c:Crash => crash = true
    case (cp:CarPosition, speed:Double) =>
      val piece = Track.pieces(cp.piecePosition.pieceIndex)
      val section = sectionOf(piece)
      if (section != currentSection) {
        if (currentSection != null) {
          updateTarget(piece, currentLane, maxSpeed, maxSlippage, crash)
          maxSpeed = 0.0
          maxSlippage = 0.0
          crash = false
        }
      }

      currentSection = section //cp.piecePosition.pieceIndex
      currentLane = cp.piecePosition.lane.endLaneIndex
      maxSpeed = maxSpeed.max(speed)
      maxSlippage = maxSlippage.max(cp.angle.abs)

    case gi:GameInit =>

      sections = Track.pieces.map(initialSectionFor)

      sections = mergeSections(sections)

//    println(sections.map(_.describe).mkString("Sections:\n", "\n", ""))

      trackFriction(0.29)


      for {s <- sections
           p <- s.pieces}
      {
        pieceMap = pieceMap + (p -> s)
      }


  }

  def sectionOf(piece:Piece) = {
    pieceMap(piece)
  }

  def mergeSections(s:List[Section]):List[Section] = {
    s match {
      case x :: Nil => List(x)
      case x :: xs =>
        val xsmerged = mergeSections(xs)
        if (x.canMergeWith(xsmerged.head)) {
          x.merge(xsmerged.head) :: xsmerged.tail
        } else {
          x :: xsmerged
        }
    }
  }

  def initialSectionFor(piece:Piece) = {
    piece match {
      case sp:StraightPiece => new Straight(sp)
      case cp:CurvedPiece => new Turn(cp)
    }
  }

  def updateSectionforPiece(piece:Piece, target:Double) {
    pieceMap(piece).setTarget(target)
  }


  def trackFriction(friction:Double) {
      println(s"Setting track friction to $friction")
      Track.friction = friction
      Track.pieces.foreach(setSpeed(_, friction))
    }

    def setSpeed(piece:Piece, friction:Double) {
      piece match {
        case sp:StraightPiece => setStraightSpeed(sp)
        case cp:CurvedPiece => setCurveSpeed(cp, friction)
      }
    }

    def setStraightSpeed(piece:StraightPiece) {
      piece.lanes.foreach(_.target.speed = 30.0)
    }

    def setCurveSpeed(piece:CurvedPiece, friction:Double) {
      for (l <- piece.lanes) {
        val laneRadius = piece.radiusOfLane(l)
        val maxLinearSpeed = Math.sqrt(friction * laneRadius)
        val angle = piece.angle

        println(s"Angle $angle, Radius $laneRadius => $maxLinearSpeed")
        l.target.speed = maxLinearSpeed
      }
      piece.friction = friction


    }

  def updateTarget(piece:Piece, lane:Int, fastest:Double, slip:Double, crash:Boolean) {
     var section = sectionOf(piece)

    section match {
      case turn:Turn =>
        var f = (fastest * fastest) / turn.radius

        if (crash) {
          f = f - 0.005
        }
        else if (fastest < piece.lanes(lane).target.speed) {
          if (slip.abs > Track.crashAngle * 0.9) {
            f = piece.friction
          } else {
            //val vdelta = piece.lanes(lane).target.speed  - fastest
            //val vdelta = Math.sin((Math.toRadians(Track.crashAngle - slip.abs)))
            //f = f + ((vdelta * vdelta) / turn.radius)
            f = f + 0.0075
          }
        }

        for {
          p <- turn.pieces
          l <- p.lanes
        } {
          val laneRadius = p.radiusOfLane(l)
                val maxLinearSpeed = Math.sqrt(f * laneRadius)
                val angle = p.angle

                println(s"Angle $angle, Radius $laneRadius => $maxLinearSpeed")
                l.target.speed = maxLinearSpeed
         p.friction = f
        }
        val firstPiece = turn.pieces(0)
        val entryPiece = firstPiece.previous

        for (i <- 0 until firstPiece.lanes.size) {
          val s0 = entryPiece.previous.lanes(i).target.speed
          val s1 = firstPiece.lanes(i).target.speed
          entryPiece.lanes(i).target.speed = s0 + ((s1 - s0) *  (2.0/ 3.0))
        }



      case _ =>
    }

    /*
    if (crash) {
//      for (i <- 0 until 3) {
        val target = piece.lanes(lane).target.speed * 0.6667
        log.info(s"Target after crash is $target for piece $piece")
      updateSectionforPiece(piece, target)
//        Track.pieces(piece - i).lanes(lane).target.speed = target

//      }
    }
    else if (slip < 40.0) {
      val percentFromMaxSlippage = slip / 40.0
      val increase = (1.0 - percentFromMaxSlippage) / 3.0
      val currentTarget = piece.lanes(lane).target.speed
      val newTarget = currentTarget * (1.0 + increase)
      log.info(s"New faster target speed for piece $piece is $newTarget")
      //Track.pieces(piece).lanes(lane).target.speed = newTarget
      updateSectionforPiece(piece, newTarget)
    } else if (slip > 50.0) {
      val currentTarget = piece.lanes(lane).target.speed
      val newTarget = currentTarget * 0.75
      log.info(s"New slower target speed for piece $piece is $newTarget")
      //Track.pieces(piece).lanes(lane).target.speed = newTarget
      updateSectionforPiece(piece, newTarget)
    } else {
      val target = piece.lanes(lane).target.speed
      log.info(s"Target is still $target for piece $piece")
    }
    */
  }
}
