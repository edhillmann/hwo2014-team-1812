package hwo2014

import akka.actor.{ActorRef, Actor, ActorLogging}

/**
 * Created by beisenmenger on 28/04/2014.
 */
class TurboActor(car:ActorRef) extends Actor with ActorLogging
{
  var turboAvailable = false
  var turboInfo:TurboAvailable = null
  var turboOn = false

  override def receive = {
    case ta:TurboAvailable =>
      turboAvailable = true
      turboInfo = ta
    case t:TurboStart =>
       turboOn = true
        turboAvailable = false
    case t:TurboEnd =>
      turboOn = false
    case cp:CarPositions =>
      if (turboAvailable && ! turboOn) {

//        val filteredList = cp.data.filter(pos => pos.id.name != Client.myName)
        cp.data.find(pos => pos.id.name == Client.myColour) match {
          case Some(me) =>
                val piece = me.piecePosition.pieceIndex
                val inPiece = me.piecePosition.inPieceDistance
                val lane = me.piecePosition.lane.endLaneIndex

                for (pos <- cp.data) {
                  if (pos.piecePosition.pieceIndex == piece
                    && pos.piecePosition.lane.endLaneIndex == lane && pos.piecePosition.inPieceDistance > inPiece) {
                    car ! Turbo("turbo", "Yeah, baby!")
                    turboAvailable = false
                  } else if (pos.piecePosition.pieceIndex == piece + 1 && pos.piecePosition.lane.endLaneIndex == lane) {
                    car ! Turbo("turbo", "Yeah, baby!")
                    turboAvailable = false
                  }
                }
          case _ =>
        }

      }
  }
}
