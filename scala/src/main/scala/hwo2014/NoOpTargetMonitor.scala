package hwo2014

import akka.actor.{ActorLogging, Actor}

/**
 * Created by beisenmenger on 20/04/2014.
 */
class NoOpTargetMonitor extends Actor with ActorLogging {

  override def receive = {
    case (cp:CarPosition, speed:Double) =>



    case gi:GameInit =>
          var sections:List[Section] = Track.pieces.map(initialSectionFor)

      //the sections are only used here currently to initialize the targets
          sections = mergeSections(sections)

//        println(sections.map(_.describe).mkString("Sections:\n", "\n", ""))

      for {s <- sections
                 p <- s.pieces}
            {
              s.initializeTargets
            }
  }

  def mergeSections(s:List[Section]):List[Section] = {
      s match {
        case x :: Nil => List(x)
        case x :: xs =>
          val xsmerged = mergeSections(xs)
          if (x.canMergeWith(xsmerged.head)) {
            x.merge(xsmerged.head) :: xsmerged.tail
          } else {
            x :: xsmerged
          }
      }
    }

    def initialSectionFor(piece:Piece) = {
      piece match {
        case sp:StraightPiece => new Straight(sp)
        case cp:CurvedPiece => new Turn(cp)
      }
    }

  def updateTarget(piece:Int, lane:Int, fastest:Double, slip:Double, crash:Boolean) {


  }
}
