package hwo2014

import akka.actor.{ActorLogging, Props, ActorSystem, Actor}
import org.json4s._
import java.net.{InetSocketAddress}
import java.io._
import org.json4s.native.Serialization
import org.json4s.native.JsonMethods._
import akka.io.{IO, Tcp}
import akka.util.ByteString
import Tcp._
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.io.Tcp.Connected
import akka.io.Tcp.Register
import akka.io.Tcp.Connect
import akka.io.Tcp.CommandFailed
import scala.Some
import akka.io.Tcp.Received

/**
 * Created by beisenmenger on 15/04/2014.
 */

object Client extends App {
  val system = ActorSystem("scarla")

  var myName: String = null
  var myColour: String = null

  args.toList match {
    case hostName :: port :: botName :: botKey :: Nil  =>
      val client = system.actorOf(Props(new Client(hostName, Integer.parseInt(port), botName, botKey)) )
      client ! Join("join", JoinData(botName, botKey))
      myName = botName

    case hostName :: port :: botName :: botKey :: track :: password :: numCars :: host =>
      val client = system.actorOf(Props(new Client(hostName, Integer.parseInt(port), botName, botKey)) )
      if (host.contains("tlimit")) {
        val throttleLimit = host(host.indexOf("tlimit") + 1).toDouble
        println("Limiting throttle to " + throttleLimit)
        client ! LimitThrottle(throttleLimit)
      }
      host.head match {
        case "host" => client ! CreateRace("createRace", RaceSetup(BotId(botName, botKey), track, password, Integer.parseInt(numCars)))
        case "guest" => client ! JoinRace("joinRace", RaceSetup(BotId(botName, botKey), track, password, Integer.parseInt(numCars)))
      }
      myName = botName

    case _ => println("args missing")
  }
}

class Client(host: String, port: Int, botName: String, botKey: String) extends Actor with ActorLogging {
  import context.system
  implicit val formats = new DefaultFormats{}


  //val bot = context.actorOf(Props(new ActorBot))
  var blackbox = context.actorOf(Props(new BlackBox(5 seconds)))
  var engine = context.actorOf(Props(new Engine(context.self)))
  log.info("Connecting...")
  IO(Tcp) ! Connect(new InetSocketAddress(host, port))

  var buffer:ByteString = null

  var bufferedMessages = List[RaceMessage]()

  override def receive = {
    case m:RaceMessage => bufferedMessages = m :: bufferedMessages
    case t: LimitThrottle => engine ! t
      case CommandFailed(_: Connect) =>
        log.warning("connect failed")
        context stop self
   
      case c @ Connected(remote, local) =>
        log.info("Connected")
        val connection = sender
        connection ! Register(self)
        //send(MsgWrapper("join", Join(botName, botKey)))

        bufferedMessages.foreach(m => context.self ! m)
        bufferedMessages = List[RaceMessage]()

        context become {
          case data: ByteString =>
            connection ! Write(data)
          case CommandFailed(w: Write) =>
            // O/S buffer was full
            log.warning( "write failed")

          case msg:MsgWrapper => send(msg)
          case msg:RaceMessage =>
            send(msg)
            blackbox ! msg
          case Received(data) =>
            if (buffer != null) {
              buffer = buffer.concat(data)
            } else {
              buffer = data
            }
            var line = buffer.decodeString("UTF8")
            if (line.endsWith("\n")) {
              val reader = new BufferedReader(new StringReader(line));
              var l = reader.readLine()
              while (l != null) {
                val json = parse(l)
                decode(json) match {
                  case Some(msg) =>
                    engine ! msg
                    //bot ! msg
                    blackbox ! msg
                    msg match {
                      case c: Crash if c.data.color == Client.myColour => blackbox ! Replay()
                      case yc: YourCar => Client.myColour = yc.data.color
                      case _ =>
                    }
                  case _ => //bot ! "Ping"
                }
                l = reader.readLine()
              }
              buffer = null
            } else {
              log.info("buffering...")
            }
          case Replay() => blackbox ! Replay()
          case "close" => connection ! Close
          case _: ConnectionClosed =>
            println("connection closed")
            context stop self
            System.exit(0)
        }
    }



  def send(msg: MsgWrapper) {
    context.self ! ByteString.fromString(Serialization.write(msg) + "\n", "UTF8")
  }

  def send(msg:RaceMessage) {
    context.self ! ByteString.fromString(Serialization.write(msg) + "\n", "UTF8")
  }

  def decode(json:JValue):Option[RaceMessage] = {
    try {
      (json \ "msgType").extract[String] match {
        case "carPositions" => Some(json.extract[CarPositions])
        case "lapFinished" => Some(json.extract[LapFinished])
        case "yourCar" => Some(json.extract[YourCar])
        case "spawn" => Some(json.extract[Spawn])
        case "crash" => Some(json.extract[Crash])
        case "gameEnd" => Some(json.extract[GameEnd])
        case "throttle" => Some(json.extract[Throttle])
        case "gameStart" => Some(json.extract[GameStart])
        case "tournamentEnd" => Some(json.extract[TournamentEnd])
        case "gameInit" => Some(json.extract[GameInit])
        case "turboAvailable" => Some(json.extract[TurboAvailable])
        case "turboStart" => Some(json.extract[TurboStart])
        case "turboEnd" => Some(json.extract[TurboEnd])
        case _ =>
          log.warning("Unidentified Message:")
          log.warning(pretty(render(json)))
          None
      }
    } catch {
      case e:Exception =>
        log.error(e, "Parse error:")
        log.error(pretty(render(json)))
        None
    }
  }


}

