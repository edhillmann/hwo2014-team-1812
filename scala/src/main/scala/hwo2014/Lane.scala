package hwo2014

/**
 * Created by ed on 16/04/14.
 */
class Lane(val index: Int, val length:Double, val distanceFromCenter:Double) {
  val target = new Target()

  def this() = this(-1, 0.0, 0.0)

}
