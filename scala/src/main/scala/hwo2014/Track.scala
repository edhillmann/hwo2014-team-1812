package hwo2014

/**
 * Created by beisenmenger on 19/04/2014.
 */
object Track {
  var trackId: Option[String] = Option.empty
  var pieces:List[Piece] = null
  var lanes = 0
  var possiblePaths: List[Path] = Nil
  var shortestPath: Option[Path] = Option.empty

  var friction:Double = 0.35   // initial value
  var crashAngle:Double = 60.0

  def init(game:GameInit) {
    if (trackId.isEmpty || !(trackId.get.eq(game.data.race.track.id))) {
      trackId = Option.apply(game.data.race.track.id)
      pieces = game.data.race.track.pieces.map(createPiece(_))
      pieces.foreach(p => p.apply(game.data.race.track.lanes))
      linkPieces(pieces)
      pieces.last.next(pieces.head)
      lanes = game.data.race.track.lanes.length
      possiblePaths = calculatePaths
      shortestPath = calcShortestPath(possiblePaths)
    }
  }

  def distanceBetween(last:PiecePosition, current:PiecePosition) = {
    if (last.pieceIndex == current.pieceIndex) {
      current.inPieceDistance - last.inPieceDistance
    } else {
      val distanceToEndOfLastPiece = pieces(last.pieceIndex).lengthOfLane(last.lane.endLaneIndex) - last.inPieceDistance
      distanceToEndOfLastPiece + current.inPieceDistance
    }
  }

  private def linkPieces(p:List[Piece]) {
    p match {
      case x :: Nil =>
      case x :: xs =>
        x.next(xs.head)
        linkPieces(xs)
    }
  }

  private def createPiece(p:PieceDataMsg) = {
    var newPiece: Piece = null
    p.radius match {
      case Some(r) => newPiece = new CurvedPiece(r, p.angle.get)
      case _ => newPiece = new StraightPiece(p.length.get)
    }
    p.switch match {
      case Some(_) => newPiece.switch = true
      case _ => newPiece.switch = false
    }
    newPiece
  }

  private def calculatePaths: List[Path] = {
    var returnValue: List[Path] = Nil
    pieces match {
      case x :: Nil =>
        returnValue = initPaths(x)
      case x :: xs =>
        returnValue = initPaths(x)
        for (p <- xs) {
          returnValue = addToPath(p, returnValue)
        }
    }
    returnValue
  }

  private def initPaths(firstPiece: Piece): List[Path] = {
    firstPiece.lanes.map(initPath)
  }

  private def initPath(lane: Lane) : Path = {
    val newPath = new Path
    newPath.addLane(lane)
    newPath
  }

  private def addToPath(nextPiece: Piece, paths: List[Path]) : List[Path] = {
    nextPiece.canSwitch match {
      case true => addSwitchingPiece(nextPiece, paths)
      case _ => addNonSwitchingPiece(nextPiece, paths)
    }
  }

  private def addSwitchingPiece(nextPiece: Piece, paths: List[Path]) : List[Path] = {
    var returnValue: List[Path] = Nil
    for (path <- paths) {
      val lastLane = path.lanes.last
      returnValue = returnValue :+ addToPath(path, lastLane)
      if (lastLane.index > 0) {
        // Add new path, where we switch to the left-hand lane
        returnValue = returnValue :+ addToPath(path, nextPiece.lanes(lastLane.index - 1))
      }
      if (lastLane.index < Track.lanes - 1) {
        // Add new path, where we switch to the right-hand lane
        returnValue = returnValue :+ addToPath(path, nextPiece.lanes(lastLane.index + 1))
      }

    }
    returnValue
  }

  private def addToPath(path: Path, lane: Lane): Path = {
    val newPath = new Path()
    newPath.copy(path)
    newPath.addLane(lane)
    newPath
  }

  private def addNonSwitchingPiece(nextPiece: Piece, paths: List[Path]) : List[Path] = {
    // Only add the length of the same lane as the last one
    for (path <- paths) {
      path.lanes match {
        case x :: Nil => path.addLane(nextPiece.lanes(x.index))
        case x :: xs => path.addLane(nextPiece.lanes(path.lanes.last.index))
      }
    }
    paths
  }

  private def sortFn(p1: Path, p2: Path): Boolean = {
    p1.length < p2.length
  }

  private def calcShortestPath(allPaths: List[Path]): Option[Path] = {
    Option.apply(allPaths.sortWith(sortFn).head)
  }

  def isOnFinalStraight(index: Int): Boolean = {
    val (x, remaining) = pieces.splitAt(index)
    remaining.forall(p => p.isInstanceOf[StraightPiece])
  }

  def describe = {
    pieces.map(p => p.describe).mkString("\n")
  }
}
