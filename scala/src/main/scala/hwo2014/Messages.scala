package hwo2014

import org.json4s._
import scala.Some
import org.json4s.native.Serialization

/**
 * Created by beisenmenger on 16/04/2014.
 */

class RaceMessage(_msgType:String)
case class Throttle(msgType:String, data:Double) extends RaceMessage(msgType)
case class GameStart(msgType:String) extends RaceMessage(msgType)
case class Switch(msgType: String, data: String) extends RaceMessage(msgType)

case class YourCar(msgType:String, data:CarId) extends RaceMessage(msgType)

case class CarPositions(msgType:String, data:List[CarPosition], gameId:String, gameTick:Int) extends RaceMessage(msgType) {
  def this(msgType:String, data:List[CarPosition], gameId:String) = this(msgType, data, gameId, -1)
}
case class CarPosition(id:CarId, angle:Double, piecePosition:PiecePosition)
case class CarId(name:String, color:String)
case class PiecePosition(pieceIndex:Int, inPieceDistance:Double, lane:LanePosition, lap:Int)
case class LanePosition(startLaneIndex:Int, endLaneIndex:Int)

case class GameEnd(msgType:String, data:RaceResult) extends RaceMessage(msgType)
case class RaceResult(results:List[CarResult], bestLaps:List[LapResult])
case class CarResult(car:CarId, result:SummaryResult)
case class SummaryResult(lap:Option[Int], laps:Option[Int], ticks:Int, millis:Long)
case class LapResult(car:CarId, result:LapSummaryResult)
case class LapSummaryResult(lap:Int, ticks:Int, millis:Long)

case class TournamentEnd(msgType:String) extends RaceMessage(msgType)
case class Crash(msgType:String, data:CarId, gameId:String, gameTick:Int) extends RaceMessage(msgType)
case class Spawn(msgType:String, data:CarId, gameId:String, gameTick:Int) extends RaceMessage(msgType)

case class LapFinished(msgType:String, data:LapInfo, gameId:String, gameTick:Int) extends RaceMessage(msgType) {
  def this(msgType:String, data:LapInfo, gameId:String) = this(msgType, data, gameId, -1)
}
case class LapInfo(car:CarId, lapTime:LapTime, raceTime:RaceTime, ranking:Ranking)
case class LapTime(lap:Int, ticks:Int, millis:Long)
case class RaceTime(laps:Int, ticks:Int, millis:Long)
case class Ranking(overall:Int, fastestLap:Int)

case class GameInit(msgType: String, data: RaceInfoMsg) extends RaceMessage(msgType)
case class RaceInfoMsg(race:RaceMsg)
case class RaceMsg(track: TrackMsg, cars: List[CarDetailsMsg], raceSession: RaceSessionMsg)
case class TrackMsg(id:String, name: String, pieces: List[PieceDataMsg], lanes: List[LaneDataMsg], startingPoint: StartingPointMsg)
case class PieceDataMsg(length: Option[Double], switch: Option[Boolean], radius: Option[Int], angle: Option[Double])
case class LaneDataMsg(distanceFromCenter: Int, index: Int)
case class StartingPointMsg(position: XYPosition, angle: Double)
case class XYPosition(x: Double, y: Double)
case class CarDetailsMsg(id: CarId, dimensions: CarDimensionsMsg)
case class CarDimensionsMsg(length: Double, width: Double, guideFlagPosition: Double)
case class RaceSessionMsg(laps: Option[Int], maxLapTimeMs: Option[Int], quickRace: Option[Boolean])

case class CreateRace(msgType:String, data:RaceSetup) extends RaceMessage(msgType)
case class RaceSetup(botId:BotId, trackName:String, password:String, carCount:Int)
case class BotId(name:String, key:String)

case class JoinRace(msgType:String, data:RaceSetup) extends RaceMessage(msgType)

case class Join(msgType:String, data:JoinData) extends RaceMessage(msgType)
case class JoinData(name:String, key:String)

//todo: Gotta take advantage of this somehow
case class TurboAvailable(msgType:String, data:TurboData, gameId:Option[String], gameTick:Option[Int]) extends RaceMessage(msgType)
case class TurboData(turboDurationMilliseconds:Double, turboDurationTicks:Option[Int], turboFactor:Double)

case class Turbo(msgType:String, data:String) extends RaceMessage(msgType)

case class TurboStart(msgType:String, data:CarId, gameId:Option[String], gameTick:Option[Int]) extends RaceMessage(msgType)
case class TurboEnd(msgType:String, data:CarId, gameId:Option[String], gameTick:Option[Int]) extends RaceMessage(msgType)
