package hwo2014

/**
 * Created by beisenmenger on 26/04/2014.
 */
object Forces {

  def centripetalForce(velocity:Double, radius:Double) = (velocity * velocity) / radius

  def angularToLinearVelocity(angularVelocityInDegrees:Double, radius:Double) = Math.toRadians(angularVelocityInDegrees) * radius

  def velocityForForce(force:Double, radius:Double) = Math.sqrt(force * radius)

}
