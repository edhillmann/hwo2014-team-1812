package hwo2014

/**
 * Created by ed on 19/04/14.
 */
class Path {

  var length: Double = 0
  var lanes: List[Lane] = Nil

  def addLane(lane: Lane) = {
    length = length + lane.length
    lanes = lanes :+ lane
  }

  def copy(path: Path) = {
    this.length = path.length
    this.lanes = path.lanes
  }
}