package hwo2014

/**
 * Created by ed on 16/04/14.
 */
class CurvedPiece(r: Int, a: Double) extends Piece {

  val radius: Int = r
  val angle: Double = a

  def apply(l:List[LaneDataMsg]) {
    val direction = angle.signum
    lanes = l.map(x => new Lane(x.index, Math.toRadians(angle.abs) * (radius - (x.distanceFromCenter * direction)), x.distanceFromCenter))
    lanes = lanes.sortWith((l0,l1) => l0.index < l1.index)
  }

  def radiusOfLane(index:Int):Double = {
    radiusOfLane(lanes(index))
  }

  def radiusOfLane(x:Lane):Double = {
    val direction = angle.signum
    radius - (x.distanceFromCenter * direction)
  }

  override def describe = "Corner " + angle + ", degrees x " + radius + ", switch " + switch
}
