package hwo2014

/**
 * Created by beisenmenger on 27/04/2014.
 */
class PID () {
  var previous_error = 0.0
  var integral = 0.0
  var setpoint = 0.0
  var error = 0.0
  var derivative = 0.0
  var Kp:Double = 0.0
  var Ki:Double = 0.0
  var Kd:Double = 0.0

  def setpoint(value:Double) {
    setpoint = value
  }

  def calculate(measured_value:Double)(implicit dt:Double = 1) = {
//    previous_error = 0
//    integral = 0
//    start:
      error = setpoint - measured_value
      integral = integral + error*dt
      derivative = (error - previous_error)/dt
    previous_error = error
    Kp*error + Ki*integral + Kd*derivative
//      wait(dt)
//      goto start
  }
}
