/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package hwo2014.strategy

class ConstantThrottleStrategy(val throttleVal: Double) extends ThrottleStrategy {
  val throttle: Double = throttleVal
  
  def getThrottle : Double = {
    return throttle
  }
}
