package hwo2014

import akka.actor.{ActorLogging, Actor}

/**
 * Created by beisenmenger on 20/04/2014.
 */
class ForceTargetMonitor extends Actor with ActorLogging {
  var currentPiece:CurvedPiece = null

  var minAngle = 0.0
  var maxAngle = 0.0
  var lastAngle = 0.0
  var angularAcceleration = 0.0
  var velocity = 0.0
  var maxVelocity = 0.0
  var acceleration = 0.0
  var incomingAcceleration = 0.0
  var lastPosition:CarPosition = null
  var dimensions:CarDimensionsMsg = null
  var learning: Boolean = true

  var minimumFriction:Option[Double] = None



  override def receive = {
    case cd:CarDimensionsMsg => dimensions = cd
    case c:Crash =>
      if (isCurved(currentPiece)) {
        val curve = currentPiece.asInstanceOf[CurvedPiece]
        setCurveSpeed(curve, minimumFriction.get)
      }
      if (isCurved(currentPiece.previous)) {
        val curve = currentPiece.previous.asInstanceOf[CurvedPiece]
        setCurveSpeed(curve, minimumFriction.get)
      }

    case (cp:CarPosition, speed:Double) =>
      val piece = Track.pieces(cp.piecePosition.pieceIndex)
      val laneindex = cp.piecePosition.lane.endLaneIndex
      val pieceIndex = cp.piecePosition.pieceIndex

      piece match {

        case curve:CurvedPiece =>
            if (curve != currentPiece) {
              if (cp.angle.abs > 15) {
                minimumFriction match {
                  case None =>
                    val f = Forces.centripetalForce(speed, curve.radiusOfLane(laneindex))
                    minimumFriction = Some(f)
                    println(s"Minimum track friction is $f")
                  case _ =>
                }
              }

              if (currentPiece != null) {


                val position = cp
                val angle = position.angle
                val lane = position.piecePosition.lane.endLaneIndex

                if (angle.abs > 50.0) {  // todo relative to track threshold?
                  var percentageDecrease = (angle.abs - 50.0) / 10.0
                  val deltaF = -0.2 * percentageDecrease * curve.friction
                  println(s"deltaf $deltaF")
                  applyToPrecedingCurves(curve, deltaF, 2)


                } else
                if (isLastPieceInCorner(curve) && angle.abs < 1 && speed > 0.9 * curve.lanes(lane).target.speed) {
                  val deltaF = curve.friction * 0.15

                  println(s"$pieceIndex friction ramp up $deltaF")
                  applyToPrecedingCurves(curve, deltaF, 2)

                }
                else if (isLastPieceInCorner(curve) && angle.abs < 35.0 && learning) {
                  var percentageIncrease = 0.8 - (angle.abs / 35.0)
                  var percentTargetSpeed = speed / curve.lanes(lane).target.speed
                  val deltaF = curve.friction * 0.1 * percentageIncrease * percentTargetSpeed
                  println(s"$pieceIndex deltaf $deltaF $percentageIncrease % increase for $angle")
                  applyToPrecedingCurves(curve, deltaF, 2)

                }
              }
              currentPiece = curve

            }


          lastPosition = cp



        case _ =>
      }


    case gi:GameInit => trackFriction(0.68)
    case ge: GameEnd => learning = false

  }

  def applyToPrecedingCurves(curve:CurvedPiece, f:Double, count:Int) {
    setCurveSpeed(curve, curve.friction + f)
    if (isCurved(curve.previous) && count > 1) {
      applyToPrecedingCurves(curve.previous.asInstanceOf[CurvedPiece], f / 2.0, count - 1)
    }
  }

  def applyToFollowingCurves(curve:CurvedPiece, f:Double, count:Int) {
      setCurveSpeed(curve, curve.friction + f)
      if (isCurved(curve.next) && count > 1) {
        applyToFollowingCurves(curve.next.asInstanceOf[CurvedPiece], f / 2.0, count - 1)
      }
    }

  def isLastPieceInCorner(piece: CurvedPiece) = {
      !isCurved(piece.next)
    }


  def trackFriction(friction:Double) {
//    println(s"Setting track friction to $friction")
    Track.friction = friction
    Track.pieces.foreach(setSpeed(_, friction))
  }

  def setSpeed(piece:Piece, friction:Double) {
    piece match {
      case sp:StraightPiece => setStraightSpeed(sp)
      case cp:CurvedPiece => setCurveSpeed(cp, friction)
    }
  }

  def setStraightSpeed(piece:StraightPiece) {
    piece.lanes.foreach(_.target.speed = 40.0)
  }

  def setCurveSpeed(piece:CurvedPiece, friction:Double) {
    for (l <- piece.lanes) {
      val laneRadius = piece.radiusOfLane(l)
      val maxLinearSpeed = Math.sqrt(friction * laneRadius)
      val angle = piece.angle
      val oldTarget = l.target.speed
      println(s"Angle $angle, Radius $laneRadius :: $oldTarget -> $maxLinearSpeed")
      l.target.speed = maxLinearSpeed
    }
    piece.friction = friction
  }

  def setSectionSpeed(piece:CurvedPiece, friction:Double) {
    var first:CurvedPiece = piece
    while (isCurved(first.previous) && angleOf(first.previous) == piece.angle) {
      first = first.previous.asInstanceOf[CurvedPiece]
    }
    setCurveSpeed(first, friction)
    while (isCurved(first.next) && angleOf(first.next) == piece.angle) {
      first = first.next.asInstanceOf[CurvedPiece]
      setCurveSpeed(first, friction)
    }
    piece.friction = friction
  }

  def isCurved(piece:Piece) = {
    piece match {
      case cp:CurvedPiece => true
      case _ => false
    }
  }

  def angleOf(piece:Piece) = {
    piece match {
          case cp:CurvedPiece => cp.angle
          case _ => 0.0
        }
  }

}
