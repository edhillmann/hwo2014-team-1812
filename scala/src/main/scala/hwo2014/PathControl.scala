package hwo2014

import akka.actor.{ActorLogging, Actor, ActorRef}

/**
 * Created by ed on 23/04/14.
 */
abstract class PathControl(car: ActorRef) extends Actor with ActorLogging {

  var switchSentForPiece: Option[Int] = Option.empty
  var newLane: Option[String] = Option.empty

  override def receive = {
    case carPositions: CarPositions =>
      val partitionedData = carPositions.data.partition(pos => pos.id.color == Client.myColour)
      val myPosition = partitionedData._1.head
      val otherPositions = partitionedData._2

      val currentPiece = Track.pieces(myPosition.piecePosition.pieceIndex)
      val switchPiece = nextSwitchPiece(currentPiece)
      val switchPieceIndex = Track.pieces.indexOf(switchPiece)
      if (switchSentForPiece.isEmpty
        || switchSentForPiece.get != switchPieceIndex) {
        newLane = calculateNewLane(myPosition, otherPositions, switchPiece)
        if (shouldSend(myPosition, switchPieceIndex)) {
          switchSentForPiece = Option.apply(switchPieceIndex)
          car ! SwitchLane(newLane.get)
        }
      }
  }

  def shouldSend(currentPosition: CarPosition, switchPieceIndex: Int): Boolean = {
    val currentPieceIndex = currentPosition.piecePosition.pieceIndex
    newLane.isDefined &&
      currentPieceIndex == switchPieceIndex - 1 &&
      (currentPosition.piecePosition.inPieceDistance > (Track.pieces(currentPieceIndex).lengthOfLane(currentPosition.piecePosition.lane.endLaneIndex) * 0.55))
  }

  def calculateNewLane(myPosition: CarPosition, otherCarPositions: List[CarPosition], switchPiece: Piece): Option[String] = {
    var newLane: Option[String] = defineOptimalLane(myPosition, otherCarPositions, switchPiece)
    val currentLaneIndex = myPosition.piecePosition.lane.endLaneIndex
    val maxLaneIdx = Track.lanes - 1

    // Check if there is a car in the target lane?
    val laneOccs = laneOccupancies(
      Track.pieces.indexOf(nextSwitchPiece(switchPiece)),
      myPosition,
      otherCarPositions
    )

    if (laneOccs._1.isDefined || laneOccs._2.isDefined || laneOccs._3.isDefined) {
      newLane match {
        case Some(direction) if ((direction == "Left") && (laneOccs._1.isDefined && laneOccs._2.isDefined && !(laneOccs._3.isDefined))) => newLane = Option.apply("Right")
        case Some(direction) if ((direction == "Left") && (laneOccs._2.isDefined)) => newLane = Option.apply("None")
        case Some(direction) if ((direction == "Right") && (laneOccs._1.isDefined && laneOccs._3.isDefined && !(laneOccs._2.isDefined))) => newLane = Option.apply("Left")
        case Some(direction) if ((direction == "Right") && (laneOccs._3.isDefined)) => newLane = Option.apply("None")
        case _ =>
      }
    }
    if (newLane.isDefined && newLane.get == "None" && laneOccs._1.isDefined) {
      // We want to stay in our current lane, but it's blocked.  See if alternative lanes are open
      if (currentLaneIndex != 0) {
        if (!(laneOccs._2.isDefined)) {
          newLane = Option.apply("Left")
        }
      }
      if ((newLane.get != "Left") && (currentLaneIndex < maxLaneIdx)) {
        newLane = Option.apply("Right")
      }
    }

    newLane.get match {
      case "Left" =>
        if (currentLaneIndex == 0) {
          newLane = Option.empty
        }
      case "Right" =>
        if (currentLaneIndex == maxLaneIdx) {
          newLane = Option.empty
        }
      case "None" => newLane = Option.empty
      case _ =>
    }
    newLane
  }

  def defineOptimalLane(myPosition: CarPosition, otherCarPositions: List[CarPosition], switchPiece: Piece): Option[String]

  def laneOccupancies(endPieceIdx: Int,
                      myPosition: CarPosition,
                      otherCarPositions: List[CarPosition]): (Option[Boolean], Option[Boolean], Option[Boolean]) = {
    var currentLane: Option[Boolean] = Option.empty
    var leftLane: Option[Boolean] = Option.empty
    var rightLane: Option[Boolean] = Option.empty

    val myLaneIdx = myPosition.piecePosition.lane.endLaneIndex
    val lastPiece = Track.pieces(endPieceIdx)
    var piece = Track.pieces(myPosition.piecePosition.pieceIndex)
    while (!(piece.equals(lastPiece))) {
      for (otherPosition <- otherCarPositions) {
        var checkLanes: Boolean = false
        otherPosition.piecePosition.pieceIndex match {
          case myPosition.piecePosition.pieceIndex =>
            checkLanes = (myPosition.piecePosition.inPieceDistance < otherPosition.piecePosition.inPieceDistance)
          case x =>
            checkLanes = (Track.pieces.indexOf(piece) == x)
        }

        if (checkLanes) {
          otherPosition.piecePosition.lane.endLaneIndex match {
            case x if (x == myLaneIdx) => currentLane = Option.apply(true)
            case x if (x == myLaneIdx - 1) => leftLane = Option.apply(true)
            case x if (x == myLaneIdx + 1) => rightLane = Option.apply(true)
            case _ =>
          }
        }
      }
      piece = piece.next
    }
    (currentLane, leftLane, rightLane)
  }

  def nextSwitchPiece(p: Piece): Piece = {
    var nextPiece = p.next
    while (!(nextPiece.canSwitch)) {
      nextPiece = nextPiece.next
    }
    nextPiece
  }

  def nextCurvedPiece(p: Piece): CurvedPiece = {
    var nextPiece = p.next
    while (!(nextPiece.isInstanceOf[CurvedPiece])) {
      nextPiece = nextPiece.next
    }
    nextPiece.asInstanceOf[CurvedPiece]
  }

}
